[![Build Status](https://travis-ci.org/montao/gamex.svg?branch=master)](https://travis-ci.org/montao/gamex) [![Coverage Status](https://coveralls.io/repos/github/montao/gamex/badge.svg?branch=master)](https://coveralls.io/github/montao/gamex?branch=master)

# gamex
Simple Android 2D game. Instructions: Just play with it. Or use the [pre-compiled apk file](https://github.com/montao/gamex/blob/master/app/release/app-release.apk).  

![](https://raw.githubusercontent.com/montao/gamex/master/jamie2d-Untitled.png)

![](https://raw.githubusercontent.com/montao/gamex/master/rymdspel.png)

 Or just use the [pre-compiled space apk file](https://github.com/montao/gamex/raw/master/app/release/space-release.apk).  
